﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LooksterServices
{
    /*
     * Service for securing token transport between clint and server
     * 
     */

    public class TokenSecureService
    {
        public bool IncodeToken(ref string _token)
        {
            //TODO: Provide a hight secure algorithm to incode
            try
            {
                _token = Reverse(_token);
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return false;
            }
            return true;
            
        }
        public bool DecodeToken(ref string _token)
        {
            //TODO: Provide a hight secure algorithm to decode
            try
            {
                _token = Reverse(_token);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return false;
            }
            return true;
        }

        public string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public string GenerateToken()
        {
            return Guid.NewGuid().ToString();
        }
        private TokenSecureService()
        {
            
        }

        private static TokenSecureService instance;

        public static TokenSecureService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TokenSecureService();
                }
                return instance;
            }
        }
    }
}
