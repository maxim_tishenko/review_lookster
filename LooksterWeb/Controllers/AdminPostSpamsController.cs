﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LooksterWeb;

namespace LooksterWeb.Controllers
{
    public class AdminPostSpamsController : Controller
    {
        private looksterdbEntities db = new looksterdbEntities();

        // GET: PostSpams
        public async Task<ActionResult> Index()
        {
            var postSpams = db.PostSpams.Include(p => p.Post).Include(p => p.User).OrderBy(x=>x.PostId);
            return View(await postSpams.ToListAsync());
        }

        // GET: PostSpams/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PostSpam postSpam = await db.PostSpams.FindAsync(id);
            if (postSpam == null)
            {
                return HttpNotFound();
            }
            return View(postSpam);
        }

        // GET: PostSpams/Create
        public ActionResult Create()
        {
            ViewBag.PostId = new SelectList(db.Posts, "PostId", "Description");
            ViewBag.UserId = new SelectList(db.Users, "UserId", "Login");
            return View();
        }

        // POST: PostSpams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "PostSpamId,PostId,UserId,Reason,CreateDate")] PostSpam postSpam)
        {
            if (ModelState.IsValid)
            {
                db.PostSpams.Add(postSpam);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.PostId = new SelectList(db.Posts, "PostId", "Description", postSpam.PostId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "Login", postSpam.UserId);
            return View(postSpam);
        }

        // GET: PostSpams/Edit/5
        public async Task<ActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PostSpam postSpam = await db.PostSpams.FindAsync(id);
            if (postSpam == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostId = new SelectList(db.Posts, "PostId", "Description", postSpam.PostId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "Login", postSpam.UserId);
            return View(postSpam);
        }

        // POST: PostSpams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "PostSpamId,PostId,UserId,Reason,CreateDate")] PostSpam postSpam)
        {
            if (ModelState.IsValid)
            {
                db.Entry(postSpam).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.PostId = new SelectList(db.Posts, "PostId", "Description", postSpam.PostId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "Login", postSpam.UserId);
            return View(postSpam);
        }

        // GET: PostSpams/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PostSpam postSpam = await db.PostSpams.FindAsync(id);
            if (postSpam == null)
            {
                return HttpNotFound();
            }
            return View(postSpam);
        }

        // POST: PostSpams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            PostSpam postSpam = await db.PostSpams.FindAsync(id);
            db.PostSpams.Remove(postSpam);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
