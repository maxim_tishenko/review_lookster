﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LooksterWeb;
using System.Diagnostics;

namespace LooksterWeb.Controllers.api
{
    public class CommentSpamsController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();

        [ResponseType(typeof(CommentSpam))]
        public async Task<IHttpActionResult> GetCommentSpam(long id)
        {
            return BadRequest();
        }

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCommentSpam(long id, CommentSpam commentSpam)
        {
            return BadRequest();
        }

        [ResponseType(typeof(CommentSpam))]
        public async Task<IHttpActionResult> PostCommentSpam(long? commentId, string reason)
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            if (commentId == null)
            {
                return BadRequest("Error! Parameter 'commentId' not provided.");
            }

            try
            {
                db.CommentSpamCreate(commentId, userId, reason);
                Debug.WriteLine(string.Format("CommentSpam Controller - CommentSpamCreate : userId {0}", userId));
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // DELETE: api/CommentSpams/5
        [ResponseType(typeof(CommentSpam))]
        public async Task<IHttpActionResult> DeleteCommentSpam(long id)
        {
            return BadRequest();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CommentSpamExists(long id)
        {
            return db.CommentSpams.Count(e => e.CommentSpamId == id) > 0;
        }
    }
}