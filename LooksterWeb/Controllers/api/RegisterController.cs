﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LooksterWeb;
using LooksterWeb.Models;
using LooksterServices;
using System.Web;
using static LooksterWeb.Controllers.PostsController;
using System.IO;
using System.Net.Mail;
using System.Diagnostics;
using LooksterWeb.Core.Email;
using LooksterWeb.Core;

namespace LooksterWeb.Controllers
{
    public class RegisterController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();


        // GET: api/Register/5
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> GetRegister(string email)
        {
            return BadRequest();
        }

        // PUT: api/Register/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRegister(long id, User user)
        {
            return BadRequest();
        }

        [ResponseType(typeof(string))]
        //public async Task<IHttpActionResult> PostRegister(RegisterUser user)
        public async Task<IHttpActionResult> PostRegister(string login, string email, string password, int age, string country, bool gender)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //Save To this server location
            var uploadPath = HttpContext.Current.Server.MapPath("~/Avatars");
            var defaultAvatarPath = HttpContext.Current.Server.MapPath("~/Content/images/avatarplch.png");
            

            //Save file via CustomUploadMultipartFormProvider
            var multipartFormDataStreamProvider = new CustomUploadMultipartFormProvider(uploadPath);
            string avatar = "";
            try
            {
                // Read the MIME multipart asynchronously 
                await Request.Content.ReadAsMultipartAsync(multipartFormDataStreamProvider);

                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

                List<string> localFileNames = multipartFormDataStreamProvider
                .FileData.Select(multiPartData => multiPartData.LocalFileName)
                .ToList();
                avatar = WebHelper.Instance.ProcessAvatarImageUrl(localFileNames, 0, uploadPath, baseUrl);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine("Image not loaded.");
                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                avatar = baseUrl + "/Content/images/avatarplch.png";
            }
            

            // Create User
            //var userId = db.UserCreate(user.Login, user.Email, user.Password, user.Age, user.Country, user.Gender).ToList();
            var userId = db.UserCreate(login, email, password,avatar,age, country, gender).ToList();
            try
            {
                await EmailSenderService.Instance.SendConfirmationEmail(userId.FirstOrDefault(), email);
                Debug.WriteLine("Send confirmation message...");
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Send confirmation message Error!\n " + ex.Message);
            }
            

            System.Diagnostics.Debug.WriteLine(string.Format("User {0} have been singned up - id: {1}", login, userId.FirstOrDefault()));
            //System.Diagnostics.Debug.WriteLine(string.Format("User {0} have been singned up - id: {1}", user.Login, userId.FirstOrDefault()));

            // Create User Token
            string newToken = TokenSecureService.Instance.GenerateToken();
            var userToken = db.TokenCreate(newToken, userId.FirstOrDefault()).ToList();
            System.Diagnostics.Debug.WriteLine(string.Format("User Token have been created - TokeID: {0}, Token: {1}", userToken.FirstOrDefault(), newToken));

            //await db.SaveChangesAsync();

            // Incode token for transfer
            TokenSecureService.Instance.IncodeToken(ref newToken);
            return Ok(newToken);
        }

        // DELETE: api/Register/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> DeleteRegister(long id)
        {
            return BadRequest();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}