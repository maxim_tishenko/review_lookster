﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LooksterWeb;
using System.Diagnostics;

namespace LooksterWeb.Controllers
{
    public class CommentsController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();


        [ResponseType(typeof(IEnumerable<CommentsByPost>))]
        public async Task<IHttpActionResult> GetComment(long postId)
        {
            List<CommentsByPost> result = null;
            try
            {
                result = db.CommentsByPost(postId).ToList();
                Debug.WriteLine(string.Format("Comments Controller - CommentsByPost : postId {0}", postId));
            }
            catch(Exception ex)
            {
                Debug.WriteLine(string.Format("Comments Controller - Comme ntsByPost : error occurs {0}", ex.Message));
                return InternalServerError(ex);
            }

            return Ok(result);
        }

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutComment(long id, String message)
        {
            return BadRequest();
        }

        [ResponseType(typeof(Comment))]
        public async Task<IHttpActionResult> PostComment(long postId,string message)
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                db.CommentCreate(message, postId, userId);
                Debug.WriteLine(string.Format("Comments Controller - CommentCreate : postId {0} userid {1}", postId,userId));
            }
            catch(Exception ex)
            {
                Debug.WriteLine(string.Format("Comments Controller - CommentCreate : error occurs {0}", ex.Message));
                return InternalServerError(ex);
            }
            return Ok();
        }

        [ResponseType(typeof(Comment))]
        public async Task<IHttpActionResult> DeleteComment(long commentId)
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            try
            {
                db.CommentDelete(commentId, userId);
            }catch(Exception ex)
            {
                return InternalServerError(ex);
            }

             return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}