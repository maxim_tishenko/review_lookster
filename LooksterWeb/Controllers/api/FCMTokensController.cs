﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LooksterWeb;
using System.Diagnostics;

namespace LooksterWeb.Controllers.api
{
    public class FCMTokensController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();

        [Route("api/Users/InvalidateFCMTokenData")]
        [HttpPost]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> InvalidateFCMTokenData(string fCMTokenData,string deviceId)
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            if(fCMTokenData == null)
            {
                return BadRequest("'fCMTokenData' parameter not provided!");
            }
            try
            {
                db.InvalidateFCMTokenData(fCMTokenData, deviceId, userId);
                return Ok("Success!");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> GetFCMToken(long id)
        {
            return BadRequest();
        }

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutFCMToken(string fCMTokenData,string deviceId)
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
          
            try
            {
                db.FCMTokenUpdate(userId, deviceId, fCMTokenData);
                Debug.WriteLine(string.Format("FCMTokensController - PutFCMToken - UserId: {0}, DeviceId: {1}", userId, fCMTokenData));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("FCMTokensController Error \n");
                Debug.WriteLine(ex.Message);
                return InternalServerError(ex);
            }

            return Ok();
        }

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PostFCMToken(string fCMTokenData,string deviceId)
        {

            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            
            try
            {
                db.FCMTokenCreate(fCMTokenData, deviceId, userId);
                System.Diagnostics.Debug.WriteLine(string.Format("FCMTokensController - PostFCMToken - UserId: {0}, DeviceId: {1}", userId, fCMTokenData));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("FCMTokensController Error \n");
                Debug.WriteLine(ex.Message);
                return InternalServerError(ex);
            }

            return Ok();
        }

        // DELETE: api/FCMTokens/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> DeleteFCMToken(long id)
        {
            //var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());

            //try
            //{
            //    db.FCMTokenDelete(fCMTokenData, deviceId, userId);
            //    System.Diagnostics.Debug.WriteLine(string.Format("FCMTokensController - PostFCMToken - UserId: {0}, DeviceId: {1}", userId, fCMTokenData));
            //}
            //catch (Exception ex)
            //{
            //    Debug.WriteLine("FCMTokensController Error \n");
            //    Debug.WriteLine(ex.Message);
            //    return InternalServerError(ex);
            //}

            //return Ok()
            return BadRequest();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}