﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LooksterWeb;
using System.Diagnostics;

namespace LooksterWeb.Controllers
{
    public class VoteController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();
        private Stopwatch timer = new Stopwatch();

        [ResponseType(typeof(IEnumerable<PostToVote>))]
        public async Task<IHttpActionResult> GetPost()
        {
            timer.Start();

            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            var votes = db.GetPostsToVote(userId);
            timer.Stop();
            Debug.WriteLine(string.Format("Vote Controller - GetPostsToVote : userId {0} Time: {1}", userId, timer.ElapsedMilliseconds.ToString()));
            return Ok(votes);
        }


        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPost(long id, Post post)
        {
            return BadRequest();
        }

        [ResponseType(typeof(Post))]
        public async Task<IHttpActionResult> PostPost(Post post)
        {
            return BadRequest();
        }


        [ResponseType(typeof(Post))]
        public async Task<IHttpActionResult> DeletePost(long id)
        {
            return BadRequest();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}