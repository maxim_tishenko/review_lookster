﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LooksterWeb;
using LooksterWeb.Core.Notifications;

namespace LooksterWeb.Controllers
{
    public class PostVotesController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();

        // GET: api/PostVotes
        [ResponseType(typeof(IEnumerable<PostVoteByPost>))]
        public async Task<IHttpActionResult> GetPostVotes(long postId)
        {
            List<PostVoteByPost> postvotes = null;

            try
            {
                postvotes = db.PostVoteByPost(postId).ToList();
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok(postvotes);
        }


        // PUT: api/PostVotes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPostVote(long id, PostVote postVote)
        {
            return BadRequest();
        }

        // POST: api/PostVotes
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PostPostVote(long postId,string photo)
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                db.PostVoteCreate(postId, userId, photo);
                var authorId = db.GetPostAuthor(postId).FirstOrDefault();
               
                if(authorId != null)
                {
                    var authorSFC = db.FCMTokenGetFCMTokenByUserId(authorId).FirstOrDefault();
                    if (authorSFC != null)
                    {
                        NotificationService nf = new NotificationService();
                        await nf.SendPushNotificationNewVotesByPost(2, authorSFC.FCMTokenData, "Ваши фото оценили!", "LooksterApp", (int)postId);

                    }

                }

            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok();
        }

        // DELETE: api/PostVotes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> DeletePostVote(long id)
        {
            return BadRequest();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}