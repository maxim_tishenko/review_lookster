﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LooksterWeb;
using System.Web;
using System.IO;
using System.Net.Http.Headers;
using System.Diagnostics;
using LooksterWeb.Core;
using LooksterWeb.Core.Notifications;

namespace LooksterWeb.Controllers
{
    public class PostsController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();
        private Stopwatch timer = new Stopwatch();

        // TODO: get posts for voting
        [ResponseType(typeof(IEnumerable<PostByUser>))]
        public async Task<IHttpActionResult> GetPosts()
        {
            timer.Start();
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            List<PostByUser> result = null;
            try
            {
                result = db.GetPostsByUser(userId).ToList();
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
            timer.Stop();
            Debug.WriteLine(string.Format("UserPost Controller - GetPostsByUser : userId {0} Time:{1}", userId, timer.ElapsedMilliseconds.ToString()));
            return Ok(result);
        }

        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPost(long postId, string description)
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            if(description==null || description == "")
            {
                return Content(HttpStatusCode.Conflict, "'description' cannot be empty!");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                db.PostUpdate(description, userId, postId);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "Some errors with updating post - postId" + postId);
            }
            return Ok();
            
        }

        [ResponseType(typeof(long))]
        public async Task<IHttpActionResult> PostPost(String description)
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            //Save To this server location
            var uploadPath = HttpContext.Current.Server.MapPath("~/Uploads");

            //Save file via CustomUploadMultipartFormProvider
            var multipartFormDataStreamProvider = new CustomUploadMultipartFormProvider(uploadPath);

            // Read the MIME multipart asynchronously 
            await Request.Content.ReadAsMultipartAsync(multipartFormDataStreamProvider);

            var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

            List<string> localFileNames = multipartFormDataStreamProvider
            .FileData.Select(multiPartData => multiPartData.LocalFileName)
            .ToList();

            long? postId = null;
            try
            {
                postId = db.PostCreate(description, WebHelper.Instance.ProcessPostImageUrl(localFileNames, 0, uploadPath, baseUrl), WebHelper.Instance.ProcessPostImageUrl(localFileNames, 1, uploadPath, baseUrl), userId).FirstOrDefault();
                NotificationService nf = new NotificationService();
                await nf.SendPushNotificationNewPostsByUser(1, "/topics/global", "Доступны новые голосования!", "LooksterApp", (int)userId);
                return Ok(postId);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Content(HttpStatusCode.InternalServerError, "Some error with post upload - UserId: " + userId);
            }
            
        }

        [ResponseType(typeof(Post))]
        public async Task<IHttpActionResult> DeletePost(long postId)
        {
            Post post = await db.Posts.FindAsync(postId);
            if (post == null)
            {
                return NotFound();
            }

            db.Posts.Remove(post);
            await db.SaveChangesAsync();

            return Ok(post);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}