﻿using LooksterWeb.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace LooksterWeb.Controllers
{
    // Attention Experimental Controller - do not use

    [RoutePrefix("api/post")]
    public class PostController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();

        [Route("{postId}/comments")]
        [HttpGet]
        [ResponseType(typeof(IEnumerable<CommentsByPost>))]
        public async Task<IHttpActionResult> GetCommentByPost(long postId)
        {
            List<CommentsByPost> result = null;
            try
            {
                result = db.CommentsByPost(postId).ToList();
                Debug.WriteLine(string.Format("Comments Controller - CommentsByPost : postId {0}", postId));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("Comments Controller - Comme ntsByPost : error occurs {0}", ex.Message));
                return InternalServerError(ex);
            }

            return Ok(result);
        }

        [Route("{postId}/votes")]
        [HttpGet]
        [ResponseType(typeof(IEnumerable<PostVoteByPost>))]
        public async Task<IHttpActionResult> GetPostVotesByPost(long postId)
        {
            List<PostVoteByPost> postvotes = null;

            try
            {
                postvotes = db.PostVoteByPost(postId).ToList();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok(postvotes);
        }


        [Route("{postId}/vote/")]
        [HttpPost]
        [ResponseType(typeof(PostVote))]
        public async Task<IHttpActionResult> VoteToPost(long postId, string photo)
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                db.PostVoteCreate(postId, userId, photo);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            return Ok();
        }


        [Route("{postId}")]
        [HttpGet]
        [ResponseType(typeof(Post))]
        public async Task<IHttpActionResult> GetPost(long postId)
        {
            Post post = await db.Posts.FindAsync(postId);
            if (post == null)
            {
                return NotFound();
            }

            return Ok(post);
        }

        [Route("")]
        [HttpPost]
        [ResponseType(typeof(long))]
        public async Task<IHttpActionResult> CreatePost(String description)
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            //Save To this server location
            var uploadPath = HttpContext.Current.Server.MapPath("~/Uploads");

            //Save file via CustomUploadMultipartFormProvider
            var multipartFormDataStreamProvider = new CustomUploadMultipartFormProvider(uploadPath);

            // Read the MIME multipart asynchronously 
            await Request.Content.ReadAsMultipartAsync(multipartFormDataStreamProvider);

            var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

            List<string> localFileNames = multipartFormDataStreamProvider
            .FileData.Select(multiPartData => multiPartData.LocalFileName)
            .ToList();

            long? postId = null;
            try
            {
                postId = db.PostCreate(description, 
                    WebHelper.Instance.ProcessPostImageUrl(localFileNames, 0, uploadPath, baseUrl), 
                    WebHelper.Instance.ProcessPostImageUrl(localFileNames, 1, uploadPath, baseUrl), userId).FirstOrDefault();
                return Ok(postId);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return Content(HttpStatusCode.InternalServerError, "Some error with post upload - UserId: " + userId);
            }



        }

        [Route("")]
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> EditPost(long postId, string description)
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            if (description == null || description == "")
            {
                return Content(HttpStatusCode.Conflict, "'description' cannot be empty!");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                db.PostUpdate(description, userId, postId);
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "Some errors with updating post - postId" + postId);
            }
            return Ok();

        }

        [Route("")]
        [HttpDelete]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> DeletePost(long postId)
        {
            Post post = await db.Posts.FindAsync(postId);
            if (post == null)
            {
                return NotFound();
            }

            db.Posts.Remove(post);
            await db.SaveChangesAsync();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
