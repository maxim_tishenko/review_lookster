﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LooksterWeb;
using System.Diagnostics;
using System.Web;
using System.Net.Http.Headers;
using System.IO;
using LooksterWeb.Core;

namespace LooksterWeb.Controllers
{
    public class UsersController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();

        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> GetUser()
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            Debug.WriteLine(string.Format("UserPost Controller - GetUser : userId {0}", userId));
            UserGetModel user = db.UserGet(userId).FirstOrDefault();
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUser(User user)
        {
            return BadRequest();
        }

        // POST: api/Users : Update User Avatar Action
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PostUser()
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            var uploadPath = HttpContext.Current.Server.MapPath("~/Avatars");
            //Save file via CustomUploadMultipartFormProvider
            var multipartFormDataStreamProvider = new CustomUploadMultipartFormProvider(uploadPath);
            string avatar = "";
            try
            {
                // Read the MIME multipart asynchronously 
                await Request.Content.ReadAsMultipartAsync(multipartFormDataStreamProvider);

                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);

                List<string> localFileNames = multipartFormDataStreamProvider
                .FileData.Select(multiPartData => multiPartData.LocalFileName)
                .ToList();
                avatar = WebHelper.Instance.ProcessAvatarImageUrl(localFileNames, 0, uploadPath, baseUrl);
                db.UpdateAvatarByUserId(userId, avatar);
                Debug.WriteLine("Image loaded sucess! userId: "+ userId);
                return Ok("User Avatar was changed.");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine("Image not loaded.");
                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                avatar = baseUrl + "/Content/images/avatarplch.png";
                return InternalServerError(ex);
            }
            
        }


        [Route("api/Users/RemoveAvatar")]
        [HttpDelete]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> RemoveAvatar()
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            var uploadPath = HttpContext.Current.Server.MapPath("~/Avatars");
            //Save file via CustomUploadMultipartFormProvider
            var multipartFormDataStreamProvider = new CustomUploadMultipartFormProvider(uploadPath);
            string avatar = "";
            try
            { 
                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                avatar = baseUrl + "/Content/images/avatarplch.png";

                db.UpdateAvatarByUserId(userId, avatar);
                Debug.WriteLine("Image loaded success! userId: " + userId);
                return Ok("User Avatar was changed.");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Debug.WriteLine("Image not loaded.");
                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                avatar = baseUrl + "/Content/images/avatarplch.png";
                return InternalServerError(ex);
            }

        }

        [Route("api/Users/UpdateUsername")]
        [HttpPost]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> UpdateUsername(string username)
        {
            if (username == null)
            {
                return BadRequest("Error! Parameter 'username' not provided.");
            }
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            try
            {
                var result = db.CheckUsernameUnic(username).FirstOrDefault();
                if(result<1)
                {
                    db.UpdateUsernameByUserId(userId, username);
                    return Ok("Success!");
                }
                else
                {
                    return Content(HttpStatusCode.Conflict, "Username - " + username  + " already exixts");
                }
                
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        [Route("api/Users/UserInsertDailyBonusAction")]
        [HttpPost]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> UserInsertDailyBonusAction()
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            try
            {
                db.UserInsertDailyBonusAction(userId);
                return Ok("Success!");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> DeleteUser()
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            try
            {
                db.UserDelete(userId);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return InternalServerError();
            }

            return Ok("User has been Deleted.");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}