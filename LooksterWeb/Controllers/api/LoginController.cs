﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LooksterWeb;
using LooksterWeb.Models;
using LooksterServices;
using System.Diagnostics;
using System.Net.Mail;
using LooksterWeb.Core.Email;
using LooksterWeb.Core;

namespace LooksterWeb.Controllers
{
    public class LoginController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();


        // GET: api/Register/5
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> GetLogin(string email)
        {
            return BadRequest();
        }

        // PUT: api/Register/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutLogin(long id, User user)
        {
            return BadRequest();
        }

        // POST: api/Register
        [ResponseType(typeof(string))]        
        public async Task<IHttpActionResult> PostLogin(string email, string password)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            UserLoginModel userLogin = null;
            // Find User
            userLogin = db.UserLogin(email, password).ToList().FirstOrDefault();
            if(userLogin==null)
            {
                return Content(HttpStatusCode.NotFound, "User not found. ");
            }
            Debug.WriteLine(string.Format("User {0} have been singned in - id: {1}", email, userLogin.UserId));

            // Create User Token
            string userToken = db.TokenGetTokenByUserId(userLogin.UserId).ToList().FirstOrDefault();
            if(userToken == null)
            {
                return Content(HttpStatusCode.InternalServerError, "User token not found. (not allowed error!)");
            }
            Debug.WriteLine(string.Format("User Token have been found - UserLogin: {0}, Token: {1}", userLogin.Login, userToken));

            TokenSecureService.Instance.IncodeToken(ref userToken);
           
            return Ok(userToken);
        }

        // DELETE: api/Register/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> DeleteLogin(long id)
        {
            return BadRequest();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        
    }
}