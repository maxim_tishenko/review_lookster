﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LooksterWeb;
using System.Diagnostics;

namespace LooksterWeb.Controllers
{
    public class PostSpamsController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();


        // PUT: api/PostSpams/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPostSpam(long id, PostSpam postSpam)
        {
            return BadRequest();
        }

        // POST: api/PostSpams
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PostPostSpam(long? postId,string reason)
        {
            var userId = Convert.ToInt64(Request.Properties["UserId"].ToString());
            if (postId == null)
            {
                return BadRequest("Error! Parameter 'postId' not provided.");
            }

            try
            {
                db.PostSpamCreate(postId, userId, reason);
                Debug.WriteLine(string.Format("PostSpam Controller - PostSpamCreate : userId {0}", userId));
                return Ok();
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [ResponseType(typeof(PostSpam))]
        public async Task<IHttpActionResult> DeletePostSpam(long id)
        {
            return BadRequest();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}