﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LooksterWeb;
using System.Net.Mail;
using System.Diagnostics;
using LooksterWeb.Core.Email;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using LooksterServices;
using LooksterWeb.Core;
using LooksterWeb.Core.ActionToken;

namespace LooksterWeb.Controllers
{
    public class ServiceController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();

        [Route("api/Service/RequestResetPassword")]
        [HttpGet]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> RequestResetPassword(string email)
        {
            GetUserByEmailModel data = db.GetUserByEmail(email).FirstOrDefault();
            if (data != null)
            {
                string ActionToken = TokenSecureService.Instance.GenerateToken();
                try
                {
                    db.ActionTokenCreate(ActionToken, data.UserId, (int)ActionTokenReasons.ResetPassword);
                    CacheProvider.Instance.SetCacheObject(email, ActionTokenReasons.ResetPassword, ActionToken);
                    TokenSecureService.Instance.IncodeToken(ref ActionToken);
                    await EmailSenderService.Instance.SendResetPasswordEmail(ActionToken, email);
                    
                    return Ok(ActionToken);
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
            return Content(HttpStatusCode.NotFound, "Error! User : " + email + " - not found!");
        }

        [Route("api/Service/RequestResendConfirmationEmail")]
        [HttpGet]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> RequestResendConfirmationEmail(string email)
        {
            int tryCounter = 0;

            if (CacheProvider.Instance.GetCacheObject(email,ActionTokenReasons.ResendConfirmationEmail) != null)
            {
                tryCounter = (int)CacheProvider.Instance.GetCacheObject(email, ActionTokenReasons.ResendConfirmationEmail);
                    if (tryCounter > 2)
                    {
                        return Content(HttpStatusCode.Forbidden, "The limit of resend confirmation emails was reached!");
                    }
                    else
                    {
                        CacheProvider.Instance.SetCacheObject(email, ActionTokenReasons.ResendConfirmationEmail, tryCounter + 1);
                    }
                        
            }else
            {
                CacheProvider.Instance.SetCacheObject(email, ActionTokenReasons.ResendConfirmationEmail, 0);
            }

            var userData = db.GetUserByEmail(email).FirstOrDefault();
            if (userData == null)
                return Content(HttpStatusCode.BadRequest, "Email not found :" + email);
            var userId = userData.UserId;
            try
            {
                await EmailSenderService.Instance.SendConfirmationEmail(userId, email);
                Debug.WriteLine("Send confirmation message...");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Send confirmation message Error!\n " + ex.Message);
                return InternalServerError(ex);
            }
            return Ok("Success!");
        }

        [Route("api/Service/CheckEmailExists")]
        [HttpGet]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> CheckEmailExists(string email)
        {
            if (email == null)
            {
                return BadRequest("Error! Parameter 'email' not provided.");
            }
            try
            {
                int? result = db.CheckUserByEmail(email).FirstOrDefault();
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Some Error with database");
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("api/Service/CheckUsernameExists")]
        [HttpGet]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> CheckUsernameExists(string username)
        {
            if (username == null)
            {
                return BadRequest("Error! Parameter 'username' not provided.");
            }
            try
            {
                int? result = db.CheckUsernameUnic(username).FirstOrDefault();
                if (result != null)
                {
                    return Ok(result);
                }
                else
                {
                    return Content(HttpStatusCode.BadGateway, "Some Error with database");
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}