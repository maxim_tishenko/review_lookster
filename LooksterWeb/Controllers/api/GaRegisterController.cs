﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LooksterWeb;
using LooksterWeb.Models;
using LooksterServices;
using System.Web;
using static LooksterWeb.Controllers.PostsController;
using System.IO;
using System.Net.Mail;
using System.Diagnostics;
using LooksterWeb.Core.Email;
using LooksterWeb.Core;
using LooksterWeb.Core.ActionToken;

namespace LooksterWeb.Controllers
{
    public class GaRegisterController : ApiController
    {
        private looksterdbEntities db = new looksterdbEntities();


        

        // GET: api/Register
        public IHttpActionResult GetUsers()
        {
            return BadRequest();
        }

        // GET: api/Register/5
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> GetRegister(string email)
        {
            return BadRequest();
        }

        // PUT: api/Register/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRegister(long id, User user)
        {
            return BadRequest();
        }

        // POST: api/Register
        [ResponseType(typeof(string))]
        //public async Task<IHttpActionResult> PostRegister(RegisterUser user)
        public async Task<IHttpActionResult> PostRegister(string login, string email, string password, int age, string country, bool gender,string avatar)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(avatar == "null")
            {
                var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority);
                avatar = baseUrl + "/Content/images/avatarplch.png";
            }
            //var userId = db.UserCreate(user.Login, user.Email, user.Password, user.Age, user.Country, user.Gender).ToList();
            var userId = db.UserCreateGA(login, email, password, avatar, age, country, gender).ToList();
          
            System.Diagnostics.Debug.WriteLine(string.Format("User {0} have been singned up - id: {1}", login, userId.FirstOrDefault()));
            //System.Diagnostics.Debug.WriteLine(string.Format("User {0} have been singned up - id: {1}", user.Login, userId.FirstOrDefault()));

            // Create User Token
            string newToken = TokenSecureService.Instance.GenerateToken();
            var userToken = db.TokenCreate(newToken, userId.FirstOrDefault()).ToList();
            System.Diagnostics.Debug.WriteLine(string.Format("User Token have been created - TokeID: {0}, Token: {1}", userToken.FirstOrDefault(), newToken));

            //await db.SaveChangesAsync();

            // Incode token for transfer
            TokenSecureService.Instance.IncodeToken(ref newToken);
            return Ok(newToken);
        }


        [Route("api/GaRegister/RequestGALogin")]
        [HttpPost]
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> RequestGALogin(string email)
        {
            if (email == null)
            {
                return BadRequest("Error! Parameter 'email' not provided.");
            }
            GetUserByEmailModel data = db.GetUserByEmail(email).FirstOrDefault();
            if (data != null)
            {
                string ActionToken = TokenSecureService.Instance.GenerateToken();
                try
                {
                    db.ActionTokenCreate(ActionToken, data.UserId, (int)ActionTokenReasons.GALogin);
                    CacheProvider.Instance.SetCacheObject(email ,ActionTokenReasons.GALogin, ActionToken);
                    TokenSecureService.Instance.IncodeToken(ref ActionToken);
                    //await EmailSenderService.Instance.SendResetPasswordEmail(ActionToken, email);

                    return Ok(ActionToken);
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
            return Content(HttpStatusCode.NotFound, "Error! User : " + email + " - not found!");
        }

        [Route("api/GaRegister/GALogin")]
        [HttpPost]
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> GALogin(string email,string actionToken)
        {
            if (email == null)
            {
                return BadRequest("Error! Parameter 'email' not provided.");
            }
            try
            {
                TokenSecureService.Instance.DecodeToken(ref actionToken);
                string storedToken = (string)CacheProvider.Instance.GetCacheObject(email,ActionTokenReasons.GALogin);
                ActionTokenGetModel actionTokenData = db.ActionTokenGet(actionToken).FirstOrDefault();
                if (storedToken != null &&
                    actionTokenData != null &&
                    actionTokenData.TokenData == storedToken &&
                    actionTokenData.IsExpired > 0 &&
                    actionTokenData.Action == (int)ActionTokenReasons.GALogin)
                {
                    UserLoginGAModel user = db.UserLoginGA(email).FirstOrDefault();
                    string userToken = db.TokenGetTokenByUserId(user.UserId).ToList().FirstOrDefault();
                    if (userToken == null)
                    {
                        return Content(HttpStatusCode.InternalServerError, "User token not found. (not allowed error!)");
                    }
                    Debug.WriteLine(string.Format("User Token have been found - UserId: {0}, Token: {1}", user.UserId, userToken));

                    TokenSecureService.Instance.IncodeToken(ref userToken);

                    //await db.SaveChangesAsync();
                    return Ok(userToken);
                }
                else
                {
                    return Content(HttpStatusCode.NotFound, "Error! User : " + email + " - not found!");
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            
        }


        // DELETE: api/Register/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> DeleteRegister(long id)
        {
            return BadRequest();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}