﻿using LooksterWeb.Core.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LooksterWeb.Controllers
{
    public class ScheduleController : Controller
    {
        private looksterdbEntities db = new looksterdbEntities();
        // GET: Schedule
        public async Task<ActionResult> Index(int id)
        {
            if (id == 14885051)
            {
                db.ResetDailyBonus();
                NotificationService ns = new NotificationService();
                await ns.SendPushNotification(0,"/topics/global","Ежедневный бонус восстановлен!", "LooksterApp - Bonus Notofication");
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> Remember(int id)
        {
            if (id == 14885051)
            {
                db.ResetDailyBonus();
                NotificationService ns = new NotificationService();
                await ns.SendPushNotification(0,"/topics/global", "Есть что сравнить? Сделай это в LooksterApp!", "LooksterApp - Какое фото лучше?");
            }
            return RedirectToAction("Index", "Home");
        }
    }
}