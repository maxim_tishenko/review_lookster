﻿using LooksterServices;
using LooksterWeb.Core;
using LooksterWeb.Core.ActionToken;
using LooksterWeb.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LooksterWeb.Controllers
{
    public class TransactionsController : Controller
    {
        private looksterdbEntities db = new looksterdbEntities();
        // GET: Transactions
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }
        public ActionResult Error()
        {
            return View("ErrorTransaction");
        }
        public ActionResult Success()
        {
            return View("SuccessTransaction");
        }
        [HttpGet]
        public ActionResult ResetPassword(string Email,string ActionTokenData)
        {
            try
            {
                string storedToken = (string)CacheProvider.Instance.GetCacheObject(Email, ActionTokenReasons.ResetPassword);
                TokenSecureService.Instance.DecodeToken(ref ActionTokenData);
                ActionTokenGetModel actionToken = db.ActionTokenGet(ActionTokenData).FirstOrDefault();
                if (storedToken != null && 
                    storedToken == ActionTokenData &&
                    actionToken.IsExpired > 0 && 
                    actionToken.Action == (int)ActionTokenReasons.ResetPassword)
                {
                    return View(new ResetPasswordModel() { Email = Email });
                }
                return View("ErrorTransaction");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return View("ErrorTransaction");
            }
            
        }

        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            try
            {
                string storedToken = (string)CacheProvider.Instance.GetCacheObject(model.Email, ActionTokenReasons.ResetPassword);
                ActionTokenGetModel actionToken = db.ActionTokenGet(storedToken).FirstOrDefault();
                if (storedToken != null &&
                    actionToken != null &&
                    actionToken.IsExpired > 0 &&
                    actionToken.Action == (int)ActionTokenReasons.ResetPassword)
                {
                        db.UpdatePasswordByUserId(actionToken.UserId, model.Password);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return View("ErrorTransaction");
            }

            //db.UpdatePasswordByUserId()
            //ViewBag.InfoText = "  ";
            return View("SuccessTransaction");
        }

        [HttpGet]
        public ActionResult ConfirmRegistrationd(long? id)
        {
            try
            {
                db.ConfirmRegistration(id);

                return View();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return View("ErrorTransaction");
            }
            
        }
    }
}