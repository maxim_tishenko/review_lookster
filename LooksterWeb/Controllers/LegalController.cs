﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LooksterWeb.Controllers
{
    public class LegalController : Controller
    {
        // GET: Legal
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TermsOfUse()
        {
            ViewBag.InfoText = "  ";
            return View();
        }
        public ActionResult PrivacyPolicy()
        {
            ViewBag.InfoText = "  ";
            return View();
        }
    }
}
