﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LooksterWeb.Core.ActionToken
{
    public enum ActionTokenReasons
    {
        GARegister = 2,
        GALogin = 3,
        ResetPassword = 4,
        ResendConfirmationEmail = 5,
    }
}