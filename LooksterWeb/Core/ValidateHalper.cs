﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace LooksterWeb.Core
{
    public static class ValidateHalper
    {
        public static bool ValidateString(string value,string name)
        {
            if (value == null)
            {
                Debug.WriteLine(string.Format("The object {0} is {1}", name, "Null!"));
                return false;
            }
            if (value == "")
            {
                Debug.WriteLine(string.Format("The object {0} is {1}", name, "Empty!"));
                return false;
            }
            return true;
        }
        public static bool ValidateString(string value, string name, int length)
        {
            if(!ValidateString(value,name))
            {
                return false;
            }
            if (value.Length != length)
            {
                Debug.WriteLine(string.Format("The object length is {0}", value));
                Debug.WriteLine(string.Format("The object {0} is {1}", name, "Length incorrect!"));
                return false;
            }
            return true;
        }
    }

}