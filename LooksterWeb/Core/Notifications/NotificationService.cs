﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace LooksterWeb.Core.Notifications
{
    public class NotificationService
    {
        public async Task SendPushNotification(int codeMsg,string toMsg,string messageMsg,string titleMsg)
        {
            try
            {
                string applicationID = "~";

                string senderId = "~";

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var dataBundle = new
                {
                    to = toMsg,
                    data = new {
                        code = codeMsg,
                        title = titleMsg,
                        message = messageMsg
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(dataBundle);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }
        public async Task SendPushNotificationNewVotesByPost(int codeMsg, string toMsg, string messageMsg, string titleMsg,int postIdMsg)
        {
            try
            {
                string applicationID = "~";

                string senderId = "~";

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var dataBundle = new
                {
                    to = toMsg,
                    data = new
                    {
                        code = codeMsg,
                        title = titleMsg,
                        message = messageMsg,
                        postIdMsg = postIdMsg
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(dataBundle);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }
        public async Task SendPushNotificationNewPostsByUser(int codeMsg, string toMsg, string messageMsg, string titleMsg, int userIdMsg)
        {
            try
            {
                string applicationID = "~";

                string senderId = "";

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var dataBundle = new
                {
                    to = toMsg,
                    data = new
                    {
                        code = codeMsg,
                        title = titleMsg,
                        message = messageMsg,
                        userId = userIdMsg
                    }
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(dataBundle);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
        }
    }
}