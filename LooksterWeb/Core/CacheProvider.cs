﻿using LooksterWeb.Core.ActionToken;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Caching;
using System.Web;

namespace LooksterWeb.Core
{
    public class CacheProvider
    {
        static CacheProvider instance;
        public static CacheProvider Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new CacheProvider();
                }
                return instance;
            }
        }
        private CacheProvider()
        {

        }


        private ObjectCache _cache = MemoryCache.Default;
        private object _lock = new object();

        // NOTE: The country parameter would typically be a database key type,
        // (normally int or Guid) but you could still use it to build a unique key using `.ToString()`.
        public object GetCacheObject(string key, ActionTokenReasons reason)
        {
            // Key can be any string, but it must be both 
            // unique across the cache and deterministic
            // for this function.
            Debug.WriteLine("Try to get the object from the cache");
            // Try to get the object from the cache
            Object getObject = _cache[key + Enum.GetName(typeof(ActionTokenReasons), reason)];

            return getObject;
        }

        public object GetCacheObject(string key)
        {
            // Key can be any string, but it must be both 
            // unique across the cache and deterministic
            // for this function.
            Debug.WriteLine("Try to get the object from the cache");
            // Try to get the object from the cache
            Object getObject = _cache[key];

            return getObject;
        }
        public void SetCacheObject(string key, object data)
        {
            // Key can be any string, but it must be both 
            // unique across the cache and deterministic
            // for this function.
            Debug.WriteLine("Try to get the object from the cache");
            // Try to get the object from the cache
            _cache.Set(key, data, DateTimeOffset.Now.AddMinutes(5));
        }

        public void SetCacheObject(string key, ActionTokenReasons reason, object data)
        {
            // Key can be any string, but it must be both 
            // unique across the cache and deterministic
            // for this function.
            Debug.WriteLine("Try to get the object from the cache");
            // Try to get the object from the cache
            _cache.Set(key + Enum.GetName(typeof(ActionTokenReasons), reason), data, DateTimeOffset.Now.AddMinutes(5));
        }
        public void SetCacheObject(string key, object data, DateTimeOffset offset)
        {
            // Key can be any string, but it must be both 
            // unique across the cache and deterministic
            // for this function.
            Debug.WriteLine("Try to get the object from the cache");
            // Try to get the object from the cache
            _cache.Set(key, data, offset);
        }

    }

}

