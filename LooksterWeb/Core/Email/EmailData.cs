﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace LooksterWeb.Core
{
	public static class EmailData
	{

        /*
        <add key = "VirginiaEmail" value="virginia@mask-me.net"/>
        <add key = "VirginiaEmailLogin" value="info@mask-me.net"/>
		<add key = "VirginiaEmailPass" value="Kutuzov123$"/>
        */

        public static string VirginiaFSI
        {
            get { return ConfigurationManager.AppSettings["VirginiaFSI"]; }
        }

        public static string VitalyFSI
        {
            get { return ConfigurationManager.AppSettings["VitalyFSI"]; }
        }
        public static string VirginiaEmail
        {
			get { return ConfigurationManager.AppSettings["VirginiaEmail"]; }
        }
        public static string VirginiaEmailLogin
        {
            get { return ConfigurationManager.AppSettings["VirginiaEmailLogin"]; }
        }

        public static string VirginiaEmailPass
        {
            get { return ConfigurationManager.AppSettings["VirginiaEmailPass"]; }
        }
        //-------------------------------------------------------------------------------------

        /*
        <add key = "SalesEmail" value="sales@mask-me.net"/>
        <add key = "SalesEmailLogin" value="sales@mask-me.net"/>
        <add key = "SalesEmailPass" value="Заднепровская16$"/>
        */
        public static string SalesEmail
        {
        get { return ConfigurationManager.AppSettings["SalesEmail"]; }
        }
        public static string SalesEmailLogin
        {
        get { return ConfigurationManager.AppSettings["SalesEmailLogin"]; }
        }
        public static string SalesEmailPass
        {
        get { return ConfigurationManager.AppSettings["SalesEmailPass"]; }
        }
        //--------------------------------------------------------------------------------------
        /*
        <add key = "InfoEmail" value="info@mask-me.net"/>
        <add key = "InfoEmailLogin" value="info@mask-me.net"/>
        <add key = "InfoEmailPass" value="Kutuzov123$"/>
        */
        public static string InfoEmail
        {
            get { return ConfigurationManager.AppSettings["InfoEmail"]; }
        }
        public static string InfoEmailLogin
        {
            get { return ConfigurationManager.AppSettings["InfoEmailLogin"]; }
        }
        public static string InfoEmailPass
        {
            get { return ConfigurationManager.AppSettings["InfoEmailPass"]; }
        }
        //--------------------------------------------------------------------------------------

        public static string SmtpClient
        {
            get { return ConfigurationManager.AppSettings["SmtpClient"]; }
        }

        public static string Login
        {
            get { return ConfigurationManager.AppSettings["Login"]; }
        }

        public static string Pass
        {
            get { return ConfigurationManager.AppSettings["Pass"]; }
        }

        public static int Port
        {
            get { return int.Parse(ConfigurationManager.AppSettings["Port"].ToString()); }
        }
    }
}
