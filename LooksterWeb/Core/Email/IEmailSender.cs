﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LooksterWeb.Core.Email
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string AddressFrom, string Login, string Pass);
    }
}