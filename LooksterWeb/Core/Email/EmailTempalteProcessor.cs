﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace LooksterWeb.Core.Email
{
    public enum EmailKeyMapping
    {
        ConfirmationLink=0,
        ResetPasswordLink=1
    }
    public class EmailTempalteProcessor
    {
        static List<string> emailLinkKeys = new List<string>()
        {
            "{confirmation_link}",
            "{resetpasswordlink}"
        };
        
        public static string GetEmailTemplate(string name)
        {
            var serverPath = HttpContext.Current.Server.MapPath("~/Content/Emails");
            var templatePath = serverPath + "/" + name;
            try
            {
                if (File.Exists(templatePath))
                {
                    using (StreamReader sr = new StreamReader(templatePath))
                    {
                        string content = sr.ReadToEnd();
                        return content;
                    }
                }
                
            }
            catch(IOException ex)
            {
                Debug.WriteLine("Error while processing email - " + name);
                Debug.WriteLine(ex.Message);
            }
            return null;
        }
        public static string SetConfirmationEmailLink(string content,string link)
        {
            return content.Replace(emailLinkKeys[(int)EmailKeyMapping.ConfirmationLink], link);
        }
        public static string SetResetPasswordEmailLink(string content, string link)
        {
            return content.Replace(emailLinkKeys[(int)EmailKeyMapping.ResetPasswordLink], link);
        }
    }
}