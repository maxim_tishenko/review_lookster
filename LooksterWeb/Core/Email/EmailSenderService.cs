﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace LooksterWeb.Core.Email
{
    public class EmailSenderService
    {
        static EmailSenderService instance;
        private EmailSenderService()
        {

        }
        public static EmailSenderService Instance
        {
            get {
                if (instance == null)
                {
                    instance = new EmailSenderService();
                }
                return instance;
            }
        }

        //"<b>Hello friend!</b></br> Thank you for registration in the LooksterApp, to confirm registration, go to the link below. </br> <a href=\"looksterapp.info/Transactions/ConfirmRegistrationd/" + userId + "\">Confirm!</a>"
        public string GetConfirmationEmailLink(long? userId)
        {
            return "looksterapp.info/Transactions/ConfirmRegistrationd/" + userId;
        }
        public string GetResetPasswordEmail(string actionToken, string email)
        {
            return String.Format("looksterapp.info/Transactions/ResetPassword?email={0}&actionTokenData={1}", email, actionToken);
        }
        public async Task SendConfirmationEmail(long? userId, string email)
        {
            SmtpClient smtp = new SmtpClient("smtp.zoho.eu", 587);
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("no-reply@looksterapp.info", "$krismax2017");
            smtp.EnableSsl = true;
            MailMessage emailMes = new MailMessage("no-reply@looksterapp.info", email, "LooksterApp Registration - Confirmation Email", 
                EmailTempalteProcessor.SetConfirmationEmailLink(
                    EmailTempalteProcessor.GetEmailTemplate("Confirm-registration.html"),
                    GetConfirmationEmailLink(userId)));

            //MailMessage emailMes = new MailMessage("no-reply@looksterapp.info", email, "LooksterApp Registration - Confirmation Email", "<b>Hello friend!</b></br> Thank you for registration in the LooksterApp, to confirm registration, go to the link below. </br> <a href=\"looksterapp.info/Transactions/ConfirmRegistrationd/" + userId + "\">Confirm!</a>");
            emailMes.IsBodyHtml = true;
            smtp.Send(emailMes);
        }
        public async Task SendResetPasswordEmail(string actionToken, string email)
        {
            SmtpClient smtp = new SmtpClient("smtp.zoho.eu", 587);
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("no-reply@looksterapp.info", "$krismax2017");
            smtp.EnableSsl = true;


            MailMessage emailMes = new MailMessage("no-reply@looksterapp.info", email, "LooksterApp Service - Reset Password",
                EmailTempalteProcessor.SetResetPasswordEmailLink(
                    EmailTempalteProcessor.GetEmailTemplate("Change-password.html"),
                    GetResetPasswordEmail(actionToken, email)));
            emailMes.IsBodyHtml = true;
            smtp.Send(emailMes);
        }
    }
}