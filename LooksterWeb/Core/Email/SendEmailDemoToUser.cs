﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LooksterWeb.Core
{
    public class SendEmailDemoToUser : EmailSender
    {

        public SendEmailDemoToUser(string addressto)
            : base(addressto)
        {
            IsHtml = false;
            Subject = "LooksterApp Registration - Confirmation Email";
            Body = BuildEmail();
        }

        /// <summary>
        /// build email body
        /// </summary>
        /// <returns>email body</returns>
        private static string BuildEmail()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<b>Hello Friend!</>");
            sb.AppendLine("Thank you for registration in the LooksteApp,");
            sb.AppendLine("Thank you for registration in the LooksteApp,");
            DateTime.Now.ToShortDateString(); DateTime.Now.ToShortTimeString();
            return sb.ToString();
        }


    }
}
