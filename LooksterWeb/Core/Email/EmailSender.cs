﻿using LooksterWeb.Core.Email;
using System;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace LooksterWeb.Core
{
	public abstract class EmailSender: IEmailSender
    {
		public string AddressTo { get; set; }

		public string Subject { get; set; }

		public string Body { get; set; }

		public bool IsHtml { get; set; }

		protected EmailSender(string addressTo)
		{
			AddressTo = addressTo;
			IsHtml = false;
		}
        public async Task SendEmailAsync(string AddressFrom,string Login,string Pass)
        {
            try
            {
                using (var msg = new MailMessage())
                {
                    msg.To.Add(AddressTo);
                    msg.From = new MailAddress(AddressFrom);
                    msg.Subject = Subject;
                    msg.Body = Body;
                    msg.IsBodyHtml = IsHtml;

                    var client = new SmtpClient(EmailData.SmtpClient)
                    {
                        EnableSsl = false,
                        Port = EmailData.Port,
                        //Setup credentials to login to our sender email address ("UserName", "Password")
                        Credentials = new NetworkCredential(Login,Pass)
                    };
                    // sending email
                    client.Send(msg);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

    }
}