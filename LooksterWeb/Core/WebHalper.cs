﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace LooksterWeb.Core
{
    public class WebHelper
    {
        private WebHelper()
        {

        }
        static WebHelper instance;
        public static WebHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WebHelper();
                }
                return instance;
            }
        }
        public string ProcessAvatarImageUrl(List<string> localFileNames, int imageNum, string uploadPath, string baseUrl)
        {
            var sourcePath = uploadPath + "\\" + localFileNames[imageNum].Split('\\').Last();
            FileInfo info = new FileInfo(sourcePath);

            string destinationPath = destinationPath = uploadPath + "\\" + new Random(Environment.TickCount).Next(999999999) + info.Extension;
            while (File.Exists(destinationPath))
                destinationPath = uploadPath + "\\" + new Random(Environment.TickCount).Next(999999999) + info.Extension;
            info.MoveTo(destinationPath);

            return baseUrl + "/Avatars/" + destinationPath.Split('\\').Last();
        }
        public string ProcessPostImageUrl(List<string> localFileNames, int imageNum, string uploadPath, string baseUrl)
        {
            var sourcePath = uploadPath + "\\" + localFileNames[imageNum].Split('\\').Last();
            FileInfo info = new FileInfo(sourcePath);

            string destinationPath = destinationPath = uploadPath + "\\" + new Random(Environment.TickCount).Next(999999999) + info.Extension;
            while (File.Exists(destinationPath))
                destinationPath = uploadPath + "\\" + new Random(Environment.TickCount).Next(999999999) + info.Extension;
            info.MoveTo(destinationPath);

            return baseUrl + "/Uploads/" + destinationPath.Split('\\').Last();
        }
    }

    public class CustomUploadMultipartFormProvider : MultipartFormDataStreamProvider
    {
        public CustomUploadMultipartFormProvider(string path) : base(path) { }

        public override string GetLocalFileName(HttpContentHeaders headers)
        {
            if (headers != null && headers.ContentDisposition != null)
            {
                return headers
                .ContentDisposition
                .FileName.TrimEnd('"').TrimStart('"');
            }

            return base.GetLocalFileName(headers);
        }


    }
}