﻿using LooksterServices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Caching;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace LooksterWeb.Core.RequestProcessing
{
    public class RequestTokenHandler : DelegatingHandler
    {
        private looksterdbEntities db = new looksterdbEntities();
        protected async override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {

            Debug.WriteLine("Process request Token Handler");

            var reg = request.RequestUri.AbsoluteUri.ToLower().Contains("register");
            var log = request.RequestUri.AbsoluteUri.ToLower().Contains("login");
            var confirm = request.RequestUri.AbsoluteUri.ToLower().Contains("service"); 

            if (!reg && !log && !confirm)
            {   
                // Call the inner handler.
                IEnumerable<string> TokenHeaders;
                TokenGet userToken = null;

                // Try get Token from request
                if (!request.Headers.TryGetValues("Access-Token", out TokenHeaders))
                {
                    return request.CreateResponse(HttpStatusCode.Unauthorized, "Access-Token not provided.");
                }

                // Validate Token
                string tokenHeader = TokenHeaders.FirstOrDefault();
                if(!ValidateHalper.ValidateString(tokenHeader, "tokenHeader", Guid.NewGuid().ToString().Length))
                {
                    return request.CreateResponse(HttpStatusCode.Unauthorized, "Access-Token invalid.");
                }
                // decode token for read
                TokenSecureService.Instance.DecodeToken(ref tokenHeader);
                Debug.WriteLine(string.Format("Token received by the server - {0}", TokenHeaders.FirstOrDefault()));

                // Try get Token from db
                userToken = GetTokenCached(tokenHeader);
                if (userToken == null)
                {
                    return request.CreateResponse(HttpStatusCode.Gone, "User not found. ");
                }
                else if (userToken.IsExpired < 1)
                {
                    return request.CreateResponse(HttpStatusCode.ResetContent, "Token expired, you need to re-login again.");
                }
                request.Properties.Add(new KeyValuePair<string, Object>("UserId", userToken.UserId));
            }
            else
            {
                Debug.WriteLine("Register request received.");
            }
            var response = await base.SendAsync(request, cancellationToken);
            Debug.WriteLine("Process response Token Handler");
            return response;
        }


        private ObjectCache _cache = MemoryCache.Default;
        private object _lock = new object();

        // NOTE: The country parameter would typically be a database key type,
        // (normally int or Guid) but you could still use it to build a unique key using `.ToString()`.
        public TokenGet GetTokenCached(string tokenData)
        {
            // Key can be any string, but it must be both 
            // unique across the cache and deterministic
            // for this function.
            var key = tokenData;
            Debug.WriteLine("Try to get the object from the cache");
            // Try to get the object from the cache
            TokenGet token = _cache[key] as TokenGet;

            // Check whether the value exists
            if (token == null)
            {
                Debug.WriteLine("not cached!");
                lock (_lock)
                {
                    // Try to get the object from the cache again
                    token = _cache[key] as TokenGet;
                    Debug.WriteLine("Try to get the object from the cache");
                    // Double-check that another thread did 
                    // not call the DB already and load the cache
                    if (token == null)
                    {
                        Debug.WriteLine("not cached!");
                        // Get the list from the DB
                        token = db.TokenGet(tokenData).ToList().FirstOrDefault();
                        Debug.WriteLine("get the object from database");
                        // Add the list to the cache
                        _cache.Set(key, token, DateTimeOffset.Now.AddMinutes(5));
                    }
                }
            }
            Debug.WriteLine("get from the cache success!");

            // Return the cached list
            return token;
        }
    }


}