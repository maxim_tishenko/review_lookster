﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LooksterWeb.Models
{
    public class ResetPasswordModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string RepeatPassword { get; set; }
    }
}