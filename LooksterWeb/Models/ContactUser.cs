﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LooksterWeb.Models
{
    public class ContactUser
    {
        public string Username { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }
    }
    public class SubscribeUser
    {
        public string Email { get; set; }
    }
}
