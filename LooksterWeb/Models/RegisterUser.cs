﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LooksterWeb.Models
{
    public class RegisterUser
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string Country { get; set; }
        public bool Gender { get; set; }
    }
}