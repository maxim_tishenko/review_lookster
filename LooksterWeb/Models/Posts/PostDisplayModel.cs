﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LooksterWeb.Models.Posts
{
    public class PostDisplayModel
    {
        public long PostId { get; set; }
        public string Description { get; set; }
        public string ImageAUrl { get; set; }
        public string ImageBUrl { get; set; }
    }
}